@extends(theme('front'))

@section('content')
<?php
  
if (auth()->check()) {
  if (auth()->user()->is_admin) {
    $editUrl = action('Backend\\'.$post->permalink->type.'Controller@edit', ['id' => $post->id]);
  }
}

?>
        <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
        <section class="home">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slider">
                        <div class="conference-slider">
                            <!-- Slideshow 3 -->
                            @if (count($beritas))
                            <ul class="conference-rslide" id="conference-slider">
                              @foreach ($beritas->take(4) as $berita)
                              <li data-title="{{$berita->title}}"><a href="{{$berita->url}}"><img src="{{$berita->picture_slider}}" alt="{{$berita->title}}" title="{{$berita->title}}"></a></li>
                              @endforeach
                            </ul>
                            <!-- Slideshow 3 Pager -->
                            <ul id="slider3-pager">
                              @foreach ($beritas->take(4) as $berita)
                              <li><a href="javascript:;"><img src="{{$berita->picture_slider_mini}}" alt="{{$berita->title}}" title="{{$berita->title}}"></a></li>
                              @endforeach
                            </ul>
                            <div class="breaking-news-title">
                                <p>{{$beritas->first()->title}}</p>
                            </div>
                            @endif
                        </div> 
                    </div>  
                </div>
            </div>
            
            <!-- BERITA -->
            <div class="row">
              <div class="col-sm-12">
                <div class="main-title-head">
                  <h3>Berita Terbaru</h3>
                  <a href="{{url('berita')}}">Semua +</a>
                </div>
              </div>
            </div>
            <div class="row">
            @if (count($beritas))
            @foreach ($beritas->take(3) as $berita)
              <div class="col-sm-4">
                <a class="text-black" href="{{$berita->url}}" title="{{$berita->title}}">
                  <img class="post-thumbnail img-responsive" src="{{$berita->picture_small ?: '//placehold.it/250x150?text='.$berita->title}}" alt="{{$berita->title}}" />
                  <h4 class="post-title">{{str_limit($berita->title, 40)}}</h4>
                </a>
                <p class="post-content">
                  {{$berita->highlight}}
                </p>
              </div>
            @endforeach
            @else
              <div class="col-sm-12">
                Postingan tidak ditemukan
              </div>
            @endif
            </div>

            <!-- ARTIKEL -->
            <div class="row">
              <div class="col-sm-12">
                <div class="main-title-head">
                  <h3>Artikel Terbaru</h3>
                  <a href="{{url('artikel')}}">Semua +</a>
                </div>
              </div>
            </div>
            <div class="row">
            @if (count($artikels))
            @foreach ($artikels->take(3) as $artikel)
              <div class="col-sm-4">
                <a class="text-black" href="{{$artikel->url}}" title="{{$berita->title}}">
                  <img class="post-thumbnail img-responsive" src="{{$artikel->picture_small ?: '//placehold.it/250x150?text='.$artikel->title}}" alt="{{$berita->title}}" />
                  <h4 class="post-title">{{str_limit($artikel->title, 40)}}</h4>
                </a>
                <p class="post-content">
                  {{$artikel->highlight}}
                </p>
              </div>
            @endforeach
            @else
              <div class="col-sm-12">
                Postingan tidak ditemukan
              </div>
            @endif
            </div>

        </section>
@stop

@section('script.footer')
<script>
    $(function() {
        $("#conference-slider").responsiveSlides({
            auto: true,
            manualControls: '#slider3-pager',
            after: function(i) {
              var title = $("#conference-slider").find('li').eq(i).first().data('title');
              $('.breaking-news-title>p').text(title);
            }
        });
    });
</script>
@stop
