@extends(theme('front'))

@section('content')

<?php
  
if (auth()->check()) {
    if (auth()->user()->is_admin) {
        $adminSlug = globalParams('slug_admin', config('livecms.slugs.admin'));
        $staticpageSlug = globalParams('slug_staticpage', config('livecms.slugs.staticpage'));
        $editUrl = route($adminSlug.'.'.$staticpageSlug.'.edit', [$post->id]);
        $deleteUrl = route($adminSlug.'.'.$staticpageSlug.'.index').'#search='.rawurlencode($post->title);
    }
}
?>
         <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
        <section class="single-post">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1>{{$statis->title}}</h1>
                        @if ($statis->picture)
                        <div class="background-img">
                            <img class="img-responsive" width="100%" class="width: 100%;" alt="{{$statis->title}}" src="{{$statis->picture_large}}" alt="{{$statis->title}}">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="post-content">
                        {!!$statis->content!!}
                    </div>
                    <div class="post-comment hide">
                        <div class="box-body">
                            <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                            <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                            <span class="pull-right text-muted">127 likes - 3 comments</span>
                        </div>
                        <div class="box-footer box-comments">
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="/backend/dist/img/user3-128x128.jpg" alt="User Image">

                                <div class="comment-text">
                                    <span class="username">
                                        Maria Gonzales
                                        <span class="text-muted pull-right">8:03 PM Today</span>
                                    </span><!-- /.username -->
                                    It is a long established fact that a reader will be distracted
                                    by the readable content of a page when looking at its layout.
                                </div>
                                <!-- /.comment-text -->
                            </div>
                            <!-- /.box-comment -->
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="/backend/dist/img/user4-128x128.jpg" alt="User Image">

                                <div class="comment-text">
                                    <span class="username">
                                        Luna Stark
                                        <span class="text-muted pull-right">8:03 PM Today</span>
                                    </span><!-- /.username -->
                                    It is a long established fact that a reader will be distracted
                                    by the readable content of a page when looking at its layout.
                                </div>
                                <!-- /.comment-text -->
                            </div>
                              <!-- /.box-comment -->
                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <img class="img-responsive img-circle img-sm" src="/backend/dist/img/user4-128x128.jpg" alt="Alt Text">
                                <!-- .img-push is used to add margin to elements next to floating images -->
                                <div class="img-push">
                                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                                </div>
                            </form>
                        </div>
                        <!-- /.box-footer -->

                    </div>
                </div>
            </div>
        </section>


@endsection

@section('script')
<script>
    $(function() {
        $('.articles').jscroll();
    });
</script>
@endsection
