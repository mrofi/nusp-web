@extends(theme('front'))

@section('content')
<?php
  
if (auth()->check()) {
  if (auth()->user()->is_admin) {
    $adminSlug = globalParams('slug_admin', config('livecms.slugs.admin'));
    $gallerySlug = globalParams('slug_gallery', config('livecms.slugs.gallery'));
    $editUrl = route($adminSlug.'.'.$gallerySlug.'.index');
  }
}

?>
         <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
        <div class="row">
          <div class="col-sm-12">
            <div class="main-title-head">
              <h3>{{$title = 'Galeri Foto'}}</h3>
            </div>
          </div>
        </div>

        <section class="gallery">
        @if (count($galleries))
          <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
          <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
              <!-- The container for the modal slides -->
              <div class="slides"></div>
              <!-- Controls for the borderless lightbox -->
              <h3 class="title"></h3>
              <a class="prev">‹</a>
              <a class="next">›</a>
              <a class="close">×</a>
              <a class="play-pause"></a>
              <ol class="indicator"></ol>
              <!-- The modal dialog, which will be used to wrap the lightbox content -->
              <div class="modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" aria-hidden="true">&times;</button>
                              <h4 class="modal-title"></h4>
                          </div>
                          <div class="modal-body next"></div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left prev">
                                  <i class="glyphicon glyphicon-chevron-left"></i>
                                  Previous
                              </button>
                              <button type="button" class="btn btn-primary next">
                                  Next
                                  <i class="glyphicon glyphicon-chevron-right"></i>
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div id="links">
          @foreach ($galleries as $gallery)
          <?php $photo = $gallery->picture ? $gallery : null; ?>
              @if ($photo)
              <a href="{{$photo->picture_large}}" title="{{$photo->title}}" data-gallery>
                  <img src="{{$photo->picture_small}}" alt="{{$photo->title}}">
              </a>
              @endif
          @endforeach
          </div>
        @else
          <div class="col-sm-12">
            Gallery tidak ditemukan
          </div>
        @endif

        <ul class="pager" @if (!$galleries->nextPageUrl()) style="display: none;" @endif ><li><a href="{{$galleries->nextPageUrl()}}" rel="prev">@if ($galleries->nextPageUrl())Next @else -------- @endif</a></li> </ul>

        </section>

@endsection

@section('script.footer')
<script>
    $(function() {
        // $('.galleries').jscroll();

    });
</script>
@endsection
