@extends(theme('front'))

@section('content')
<?php
  
if (auth()->check()) {
  $userhomeSlug = globalParams('slug_userhome', config('livecms.slugs.userhome'));
  $articleSlug = globalParams('slug_article', config('livecms.slugs.article'));
  $editUrl = route($userhomeSlug.'.'.$articleSlug.'.index');
}

?>
         <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
        <div class="row">
          <div class="col-sm-12">
            <div class="main-title-head">
              <h3>{{$title}}</h3>
            </div>
          </div>
        </div>

        <section class="articles">
        @if (count($articles))
        @foreach ($articles as $article)
          <?php $c = isset($c) ? ++$c : 1; $vid = count($article->getVideoContent()) ? $article->getVideoContent()->first() : null; ?>
          @if ($c % 3 == 1) <div class="row"> @endif
          <div class="col-sm-4">
            <a class="text-black" href="{{$article->url}}" title="{{$article->title}}">
              <img class="post-thumbnail img-responsive" src="{{$vid ? $vid->getThumbnail('mqdefault') : ($article->picture_medium ?: '//placehold.it/250x150?text='.$article->title)}}" alt="{{$article->title}}" />
              <h4 class="post-title">{{$article->title}}</h4>
            </a>
            <p>
              {{$vid ? '' : str_limit($article->highlight, 150)}}
            </p>
            <p>&nbsp;</p>
          </div>
          @if ($c % 3 == 0 || $c == count($articles)) </div> @endif
        @endforeach
        @else
          <div class="col-sm-12">
            Postingan tidak ditemukan
          </div>
        @endif

        <ul class="pager" @if (!$articles->nextPageUrl()) style="display: none;" @endif ><li><a href="{{$articles->nextPageUrl()}}" rel="prev">@if ($articles->nextPageUrl())Next @else -------- @endif</a></li> </ul>

        </section>

@endsection

@section('script.footer')
<script>
    $(function() {
        // $('.articles').jscroll();
    });
</script>
@endsection
