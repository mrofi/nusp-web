@extends(theme('front'))

@section('content')

<?php

if (auth()->check()) {
if (auth()->user()->is_admin || $post->author_id == auth()->user()->id) {
  $userhomeSlug = globalParams('slug_userhome', config('livecms.slugs.userhome'));
  $articleSlug = globalParams('slug_article', config('livecms.slugs.article'));
  $editUrl = route($userhomeSlug.'.'.$articleSlug.'.edit', [$post->id]);
  $deleteUrl = route($userhomeSlug.'.'.$articleSlug.'.index').'#search='.rawurlencode($post->title);
}
}

?>
         <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
        <section class="single-post">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1>{{$article->title}}</h1>
                        <div class="post-meta text-muted">
                            <span><i class="fa fa-user"></i> {{$article->author->name}}</span>
                            <span><i class="fa fa-clock-o"></i> {{$article->published_at->diffForHumans()}}</span>
                            @if (count($article->categories))
                            <span><i class="fa fa-list"></i> Category: {{dataImplode($article->categories, 'category')}}</span>
                            @endif
                            @if (count($article->tags))
                            <span><i class="fa fa-tag"></i> Tags: {{dataImplode($article->tags, 'tag')}}</span>
                            @endif
                        </div>
                        @if ($article->picture)
                        <div class="background-img">
                            <img class="img-responsive" width="100%" class="width: 100%;" alt="{{$article->title}}" src="{{$article->picture_large}}" alt="{{$article->title}}">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="post-content @if (!$article->picture) no-post-image @endif">
                        {!!$article->content!!}
                    </div>
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <div class="user-block">
                                <h4>Diterbitkan Oleh :</h4>
                            </div>
                            <div class="user-block">
                                @if (!$article->author->avatar)
                                <div class="user-label">
                                    <span>{{ $article->author->getInitial() }}</span>
                                </div>
                                @else
                                <img class="img-circle" src="{{$article->author->avatar}}" alt="User Image">
                                @endif
                                <span class="username"><a href="#">{{$article->author->name}}</a> 
                                @if (count($socials = $article->author->socials))
                                @foreach ($socials as $social => $url)
                                    @if ($url)
                                    <a href="{{$url}}" class="btn btn-xs btn-link" data-toggle="tooltip" title="{{title_case($social)}}"><i class="fa fa-{{$social}}"></i></a>
                                    @endif
                                @endforeach
                                @endif
                                </span>
                                <span class="description">{{$article->author->about}}</span>
                            </div>
                            <!-- /.user-block -->
                        </div>
                    </div>
                    <div class="post-comment hide">
                        <div class="box-body">
                            <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                            <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                            <span class="pull-right text-muted">127 likes - 3 comments</span>
                        </div>
                        <div class="box-footer box-comments">
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="/backend/dist/img/user3-128x128.jpg" alt="User Image">

                                <div class="comment-text">
                                    <span class="username">
                                        Maria Gonzales
                                        <span class="text-muted pull-right">8:03 PM Today</span>
                                    </span><!-- /.username -->
                                    It is a long established fact that a reader will be distracted
                                    by the readable content of a page when looking at its layout.
                                </div>
                                <!-- /.comment-text -->
                            </div>
                            <!-- /.box-comment -->
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="/backend/dist/img/user4-128x128.jpg" alt="User Image">

                                <div class="comment-text">
                                    <span class="username">
                                        Luna Stark
                                        <span class="text-muted pull-right">8:03 PM Today</span>
                                    </span><!-- /.username -->
                                    It is a long established fact that a reader will be distracted
                                    by the readable content of a page when looking at its layout.
                                </div>
                                <!-- /.comment-text -->
                            </div>
                              <!-- /.box-comment -->
                        </div>
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <img class="img-responsive img-circle img-sm" src="/backend/dist/img/user4-128x128.jpg" alt="Alt Text">
                                <!-- .img-push is used to add margin to elements next to floating images -->
                                <div class="img-push">
                                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                                </div>
                            </form>
                        </div>
                        <!-- /.box-footer -->

                    </div>
                </div>
            </div>
        </section>


@endsection

@section('script.footer')
<script>
    $(function() {
        // $('.articles').jscroll();
        $('iframe').each(function() {
            var out = $(this).parents('.post-content');
            var width = out.innerWidth();
            var height = 9/16*width;
            $(this).attr('width', width).attr('height', height);
        })
    });
</script>
@endsection
