<?php

$api = new Flagrow\Flarum\Api\Client('http://nusp-2.org/forum/api/');
$discussions = collect($api->discussions()['data']);

return [
    'beritas' => getCategory('berita', 'article', 10, ['*'], 'desc', 'published_at'),
    'artikels' => getCategory('artikel', 'article', 10, ['*'], 'desc', 'published_at'),
    'agendas' => getCategory('informasi', 'article', 10, ['*'], 'desc', 'published_at'),
    'videos' => getCategory('video', 'article', 10, ['*'], 'desc', 'published_at'),
    'discussions' => $discussions,
];
