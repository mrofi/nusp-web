  <section class="main-footer bg-navy">
      <div class="row">
        <div class="col-sm-9">
          <div class="row">
            <div class="col-xs-12">
              <div class="main-title-head">
                <h3 class="text-white">Jaringan Web NUSP</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 col-sm-2">
              <a target="_blank" rel="no-index, no-follow" href="http://ciptakarya.pu.go.id/bangkim/">
                <img class="img-responsive" src="/nusp/r1.jpg"/>
                <p class="text-center text-white text-upper">Bangkim</p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-2">
              <a target="_blank" rel="no-index, no-follow" href="http://ciptakarya.pu.go.id/v3/">
                <img class="img-responsive" src="/nusp/r2.jpg"/>
                <p class="text-center text-white text-upper">Cipta Karya</p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-2">
              <a target="_blank" rel="no-index, no-follow" href="http://pu.go.id/">
                <img class="img-responsive" src="/nusp/r3.jpg"/>
                <p class="text-center text-white text-upper">PU</p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-2">
              <a target="_blank" rel="no-index, no-follow" href="http://bappenas.go.id/id/">
                <img class="img-responsive" src="/nusp/r4.jpg"/>
                <p class="text-center text-white text-upper">Bappenas</p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-2">
              <a target="_blank" rel="no-index, no-follow" href="http://www.adb.org/">
                <img class="img-responsive" src="/nusp/r5.jpg"/>
                <p class="text-center text-white text-upper">ADB</p>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="row">
            <div class="col-xs-12">
              <div class="main-title-head">
                <h3 class="text-white">Alamat</h3>
              </div>
            </div>
          </div>
          Jl. Penjernihan I No 19 F1, <br class="visible-md visible-lg">
          Pejompongan Jakarta Pusat <br class="visible-md visible-lg">
          Telp 021-5743693, Fax. 021-5743692.
        </div>
      </div>
  </section>
  <section class="main-footer bg-navy">
    
  </section>