<div class="google-search">
<script>
  (function() {
    var cx = '005841946876046239444:up69wlvxdma';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>

<div class="box flat">
    <div class="box-body bg-gray">
        <form class="form-navbar" role="search">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Pencarian...">
              <span class="fa fa-search form-control-feedback"></span>
            </div>
          </form>
    </div>
</div>
<!-- /.box -->

<div class="row">
  <div class="col-xs-12">
    <div class="main-title-head">
      <h3>Informasi</h3>
      <a href="{{url('informasi')}}">Semua +</a>
    </div>
  </div>
</div>
@if (count($agendas))
<ul class="sidebar-post-item">
@foreach ($agendas as $agenda)
  <a href="{{$agenda->url}}">
    <li>
      <h5 class="post-title">{{$agenda->title}}</h5>
    </li>
  </a>
@endforeach
</ul>
@else
  <div>
    Postingan tidak ditemukan
  </div>
@endif

<div class="row">
  <div class="col-xs-12">
    <div class="main-title-head">
      <h3>Forum</h3>
      <a href="{{url('forum')}}">Semua +</a>
    </div>
  </div>
</div>
@if (count($discussions))
<ul class="sidebar-post-item">
@foreach ($discussions as $discussion)
  <a href="{{url('forum/d/'.$discussion['id'].'-'.$discussion['attributes']['slug'])}}">
    <li>
      <h5 class="post-title">{{$discussion['attributes']['title']}}</h5>
    </li>
  </a>
@endforeach
</ul>
@else
  <div>
    Postingan tidak ditemukan
  </div>
@endif

<div class="row">
  <div class="col-xs-12">
    <div class="main-title-head">
      <h3>Video</h3>
      <a href="{{url('video')}}">Semua +</a>
    </div>
  </div>
</div>
@if (count($videos))
@foreach ($videos as $video)
<?php $vid = count($video->getVideoContent()) ? $video->getVideoContent()->first() : null; ?>
<a href="{{$video->url}}" title="{{$video->title}}">
  <div class="row sidebar-post-item">
    <img class="col-xs-4 post-thumbnail img-responsive" src="{{$vid ? $vid->thumbnail : $video->picture_small}}" />
    <div class="col-xs-8">
      <h5 class="post-title">{{str_limit($video->title, 100)}}</h5>
      <h5 class="post-title"><small>{{$vid ? gmdate("H:i:s", $vid->duration) : ''}}</small></h5>
    </div>
  </div>
</a>
@endforeach
@else
  <div>
    Postingan tidak ditemukan
  </div>
@endif
