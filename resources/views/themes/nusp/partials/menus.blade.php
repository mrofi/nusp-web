<li class="dropdown @if (request()->is('profile') || request()->is('pengertian') || request()->is('dasar-hukum') || request()->is('struktur-organisasi')) active @endif">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">NUSP-2 <span class="caret"></span><span class="sr-only">(current)</span></a>
  <ul class="dropdown-menu" role="menu">
    <li @if (request()->is('profile')) class="active" @endif><a href="{{url('profile')}}">Profile</a></li>
    <li @if (request()->is('pengertian')) class="active" @endif><a href="{{url('pengertian')}}">Pengertian</a></li>
    <li @if (request()->is('dasar-hukum')) class="active" @endif><a href="{{url('dasar-hukum')}}">Dasar Hukum</a></li>
    <li @if (request()->is('struktur-organisasi')) class="active" @endif><a href="{{url('struktur-organisasi')}}">Struktur Organisasi</a></li>
  </ul>
</li>
<li @if (request()->is('kontak')) class="active" @endif><a href="{{url('kontak')}}">Kontak</a></li>
<li @if (request()->is('informasi')) class="active" @endif><a href="{{url('informasi')}}">Informasi</a></li>
<li @if (request()->is('galeri')) class="active" @endif><a href="{{url('galeri')}}">Galeri Foto</a></li>
<li @if (request()->is('pustaka')) class="active" @endif><a href="{{url('pustaka')}}">Pustaka</a></li>
<li @if (request()->is('lokasi-sasaran')) class="active" @endif><a href="{{url('lokasi-sasaran')}}">Lokasi Sasaran</a></li>
<li><a href="//nusp.16mb.com">SIM</a></li>
<li @if (request()->is('pengaduan')) class="active" @endif><a href="{{url('pengaduan')}}">Pengaduan</a></li>
