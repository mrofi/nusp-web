<?php
  
  use App\Models\Category;

  $adminSlug = globalParams('slug_admin', config('livecms.slugs.admin'));
  $userSlug = globalParams('slug_userhome', config('livecms.slugs.userhome'));
?>
  @if (auth()->user()->is_admin)
  <li><a href="{{url($adminSlug)}}">Dashboard</a></li>
  @endif
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Artikel <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="{{url($articleSlug = $userSlug.'/'.globalParams('slug_article', config('livecms.slugs.article')))}}">Semua Kategori</a></li>
      <li class="divider"></li>
      @if (count($categories = Category::get()))
      @foreach ($categories as $category)
      <li><a href="{{url($articleSlug.'?cat='.$category->slug)}}">{{$category->category}}</a></li>
      @endforeach
      @endif
    </ul>
  </li>
  @if (isset($editUrl))
  <li><a class="bg-red" href="{{$editUrl}}"><i class="fa fa-pencil"></i> Edit</a></li>
  @endif
  @if (isset($deleteUrl))
  <li><a class="bg-red" href="{{$deleteUrl}}"><i class="fa fa-trash-o"></i> Delete</a></li>
  @endif





