<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $title or '' }} | {{ globalParams('site_name', 'Live CMS') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="/backend/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/backend/plugins/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/backend/plugins/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="/backend/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="/backend/plugins/select2/select2-bootstrap.min.css">
  <!-- Datepicker -->
  <link rel="stylesheet" href="/backend/plugins/datepicker/datepicker3.css">
  <!-- datatables -->
  <link rel="stylesheet" href="/backend/plugins/datatables/dataTables.bootstrap.css">
  <!-- I Check -->
  <link rel="stylesheet" href="/backend/plugins/iCheck/square/blue.css">
  <!-- Styky Table Header -->
  <link rel="stylesheet" href="/backend/plugins/sticky-table-headers/css/component.css">
  <!-- SweetAlert -->
  <link rel="stylesheet" href="/backend/plugins/sweetalert/sweetalert.css">
  <!-- SweetAlert Forms -->
  <link rel="stylesheet" href="/backend/plugins/swal-forms/swal-forms.css">
  <!-- Responsive Slides -->
  <link rel="stylesheet" href="/backend/plugins/responsive-slides/responsiveslides.css">
  <!-- BluImpImage Gallery -->
  <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
  <link rel="stylesheet" href="/backend/plugins/bootstrap-image-gallery/css/bootstrap-image-gallery.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/backend/dist/css/AdminLTE.dark.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="/backend/dist/css/skins/skin-dark.min.css">

  <style type="text/css">
    @font-face {
        font-family: 'bebasregular';
        src: url('/backend/dist/fonts/BEBAS___-webfont.eot');
        src: url('/backend/dist/fonts/BEBAS___-webfont.eot?#iefix') format('embedded-opentype'),
             url('/backend/dist/fonts/BEBAS___-webfont.woff') format('woff'),
             url('/backend/dist/fonts/BEBAS___-webfont.ttf') format('truetype'),
             url('/backend/dist/fonts/BEBAS___-webfont.svg#bebasregular') format('svg');
        font-weight: normal;
        font-style: normal;
    }

    body {
        background-attachment: fixed; 
        background-size: cover;
        background-repeat: no-repeat; 
    }

    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
      font-weight: bold;
    }

    h1 {
      font-size: 2.3em;
    }

    .bg-dark-blue {
        background-color: #20409A;
        color: #fff;
    }

    .text-dark-blue {
        color: #1B5076;
    }

    .text-white {
        color: #fff!important;
    }

    .bg-dark-navy {
        background-color: #010F1D;
        color: #aaa;
    }

    .bg-dark-navy a {
        color: #666;
        text-decoration: underline;
    }

    .text-upper {
        text-transform: uppercase;
    }

    .user-label {
        width: 30px;
        height: 30px;
        text-align: center;
        background-color: rgba(85, 85, 85, 0.25);
        color: #fff;
        border-radius: 50%;
        margin-top: -5px;
        margin-bottom: -5px;
        padding: 5px;
        position: absolute;
    }

    .dropdown-toggle .user-label {
        right: 0;
        left: 0;
    }

    .user-block .user-label {
        width: 40px;
        height: 40px;
        margin-top: 0;
        padding: 10px;
    }


    .dark .user-label {
        background-color: #545353;
    }

    .jumbotron {
        margin: 0;
        border-radius: 0!important;
    }

    .jumbotron.header-image {
        background-image: url(/nusp/header.jpg);
        background-repeat: no-repeat;
        background-size: contain;
        background-color: transparent;
        background-position: center bottom;
        margin-bottom: 5px;
    }
  
    .navbar.main-menu {
        border-bottom: 0;
        margin-bottom: 0;
    }

    .navbar.main-menu a {
        font-family: 'bebasregular', Arial, Helvetica, sans-serif;
        font-size: 1.2em;
        color: #fff;
        text-transform: uppercase;
        padding-left: 1.2em;
        padding-right: 1.2em;
        /*font-weight: 400;*/
    }

    .navbar.main-menu li.active > a, .navbar.main-menu li>a:hover, .navbar.main-menu li>a:active, .navbar.main-menu li>a:focus {
        background-color: #F5DD05;
        color: #444;
    }
    
    .dropdown-menu li>a {
        padding: 10px;
    }
    
    input.form-control.main-search {
        display: inline-block;
    }

    .navbar-right {
        float: right;
        width: 70%;
        padding: 8px;
    }
    
    .wrapper {
        background-color: transparent!important;
    }

    .content-wrapper {
        background-color: transparent;
    }

    .content {
        background-color: #fff;
    }

    a.navbar-brand.active {
        background-color: #F5DD05;
        color: #444;
    }

    .nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
        background-color: #F5DD05;
        color: #444;
    }

    ul.dropdown-menu {
        background-color: #20409A;
    }

    .post-title {
        margin-top: 20px;
        line-height: 1.5em;
    }

    .post-content {
        padding-top: 2em;
        padding-bottom: 2em;
    }

    .post-content.no-post-image {
        padding-top: 0;
    }

    .home .post-content {
        padding-top: 0;
    }

    .post-meta span {
        padding-right: 20px;
        display: inline-block;
    }
    .post-meta span .fa {
        margin-right: 5px;
    }

    .post-meta {
        padding-bottom: 20px;
    }
.post-item {
  margin-bottom: 20px;
}

.slider {
  margin-bottom: 30px;
}

.conference-rslide {
  position: relative;
  list-style: none;
  overflow: hidden;
  width: 80%;
  padding: 0;
  margin: 0;
}
.conference-slider{
  position:relative;
}
.breaking-news-title {
  position: absolute;
  background:rgba(36, 34, 36, 0.68);
  bottom: 0;
  left: 0;
  z-index: 999;
  width: 80%;
  padding: 1em;
}
.breaking-news-title p {
  color:#fff;
  font-size:1em;
  font-weight:normal;
  line-height:1.7em;
}
.conference-rslide li {
  -webkit-backface-visibility: hidden;
  position: absolute;
  display: none;
  width: 100%;
  left: 0;
  top: 0;
}
.conference-rslide li:first-child {
  position: relative;
  display: block;
  float: left;
}
.conference-rslide img {
  display: block;
  float: left;
  width: 100%;
}
.rslides_tabs {
  width: 24%;
  position: absolute;
  top: 0;
  z-index: 999;
  right: 0;
}
.rslides_tabs li {
  display: block;
}
.rslides_tabs a {
  border: 3px solid #fff;
}
.rslides_tabs li:first-child {
  margin-left: 0;
}
.rslides_tabs .rslides_here a {
  color: #fff;
  font-weight: bold;
}
#slider3-pager a {
  display: inline-block;
  width: 100%;
}
#slider3-pager img {
  float: left;
  display:block;
  width: 100%;
}
#slider3-pager .rslides_here a {
  background: transparent;
  border:3px solid #F5DD05;
}
#slider3-pager a {
}

.main-title-head {
  border-bottom: 4px solid #F5DD05;
  /*margin-bottom:1.7em;*/
  margin-top: 1em;
  margin-bottom: 1em;
  position: relative;
  height: 60px;
}
.main-title-head h3 {
  color:#202021;
  font-size:1.6em;
  font-weight:400;
  word-spacing:3px;
  float:left;
  font-family: 'bebasregular';
  padding-bottom: 4px;
}
.main-title-head a {
  position: relative;
  float: right;
  text-decoration:none;
  color:#202021!important;
  background:#F5DD05;
  text-transform:uppercase;
  font-size: 1em;
  padding:3px 8px;
  top: 1.3em;
  /*bottom: 2em;*/
}

.sidebar a {
  color: #111!important;
}

.sidebar-post-item {
    margin-bottom: 10px;
}

ul.sidebar-post-item li {
    margin-left: -20px;
}

.sidebar .post-title {
    margin: 5px 0 0;
}

/*-- responsive-mediaqueries --*/
@media (max-width:768px){
  .jumbotron.header-image {
      background-image: url(/nusp/header_mobile.jpg);
  }
  #slider3-pager a {
    width: 100%;
  }
  .rslides_tabs {
    bottom: 10px;
  }
  .navbar-right {
    width: 80%;
  }
}
@media (max-width:640px){
  #slider3-pager a {
    width: 100%;
  }
  .rslides_tabs {
    bottom: 15px;
  }
  .navbar-right {
    width: 70%;
  }
  .gsc-results-wrapper-overlay.gsc-results-wrapper-visible {
      width: 90%;
      left: 4%;
  }
}
@media (max-width:480px){
  #slider3-pager a {
    width: 100%;
  }
  .rslides_tabs {
    bottom: 20px;
  }
  .navbar-right {
    width: 65%;
  }
}
@media (max-width:320px){
  #slider3-pager a {
    width: 100%;
  }
  .rslides_tabs {
    bottom: 25px;
  }
  .navbar-right {
    width: 60%;
  }
}
.confer {
  margin-bottom: 4em;
}

/*GOOGLE SEARCH*/
.google-search {
  position: absolute!important;
}
.gsc-modal-background-image {
    background-color: #20409A!important;
}

</style>

  @yield('css.header')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             | 
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
@section('templateBody')
<body class="dark {{ $bodyClass or 'skin-blue layout-top-nav' }}" style="background-image: url(/nusp/background.jpg);">
  
  @if (auth()->check())
  <style type="text/css">
      
      .adminbar a.navbar-brand {
          color: #333;
      }

      .adminbar ul.dropdown-menu {
          background-color: #FAFAFA;
      }

      .adminbar .dropdown-menu>li>a {
          color: #333;
      }

      .adminbar .navbar .dropdown-menu li a:hover {
          background: #e1e3e9!important;
          color: #333;
      }

      .adminbar .navbar .dropdown-menu li a {
          color: #333!important;
      }

      .adminbar .navbar .dropdown-menu li.divider {
          background-color: #E7DFDF!important;
      }

      .adminbar .navbar-toggle {
          color: #545353;
      }

      header.adminbar.main-header {
          position: fixed;
          width: 100%;
          top: 0;
      }

  </style>
  <header class="adminbar main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <!-- Logo -->
          <a href="{{url(globalParams('slug_admin', config('livecms.slugs.admin')))}}" class="navbar-brand">
            {{ globalParams('site_name', 'Live CMS') }}
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <!-- Sidebar Menu -->
          <ul class="nav navbar-nav">
          @section('topmenus')
            @include('themes.nusp.partials.topmenus')
          @stop
          @yield('topmenus')
          </ul>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.navbar-collapse -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a class="dropdown-toggle">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{auth()->user()->name}}</span>
              </a>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if (!auth()->user()->avatar)
                <div class="user-label">
                  <span>{{ auth()->user()->getInitial() }}</span>
                </div>
                @else
                <!-- The user image in the navbar-->
                <img src="{{auth()->user()->avatar}}" class="user-image" alt="User Image">
                @endif
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  @if (auth()->user()->avatar)
                  <img src="{{auth()->user()->avatar}}" class="img-circle" alt="User Image">
                  @else
                  <i class="text-gray ion ion-person fa-5x"></i>
                  @endif
                  <p>
                    {{ str_limit(auth()->user()->name, 20) }}
                    <small>Since {{ auth()->user()->created_at->diffForHumans() }}</small>
                  </p>
                </li>
                <!-- Menu Body -->
  <!--               <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                </li> -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{ route((site()->subfolder ? site()->subfolder.'.' : '').globalParams('slug_userhome', config('livecms.slugs.userhome')).'.'.globalParams('slug_profile', config('livecms.slugs.profile')).'.index') }}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <li><a href="{{ url('/logout') }}"><i class="fa fa-lock"></i> <span class="hidden-sm">Logout</span></a></li>
          </ul>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <div style="margin-bottom: 50px;"></div>
  @endif
<div class="wrapper">

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div class="container">
        <div class="jumbotron header-image">
          &nbsp;
        </div>

        <nav class="navbar navbar-static-top main-menu bg-dark-blue">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-header">
               <a href="{{url('/')}}" class="navbar-brand active">
                <i class="fa fa-lg fa-home"></i>
              </a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse2">
                <i class="fa fa-bars"></i>
              </button>
              <form class="navbar-right visible-xs" role="search">
                  <input type="text" class="form-control main-search" placeholder="Pencarian...">
              </form>
            </div>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse2">
              <!-- Sidebar Menu -->
              <ul class="nav navbar-nav">
              @section('menus')
                @include('themes.nusp.partials.menus')
              @stop
              @yield('menus')
              </ul>
              <!-- /.sidebar-menu -->
              
          </div>
          <!-- /.container-fluid -->
        </nav>
        
      @section('main-content')  
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-sm-8">
              @yield('content')
            </div>
            <div class="col-sm-4 sidebar">
              @include('themes.nusp.partials.sidebar')
            </div>
          </div>

        </section>
        <!-- /.content -->
      @stop
  
      @yield('main-content')
      <!-- footer -->
      @include('themes.nusp.partials.footer')

      <!-- Main Footer -->
      <footer class="main-footer bg-dark-navy">
          <!-- To the right -->
          <div class="pull-right hidden-xs">
            Made with <i class="fa fa-heart text-red"></i> in Jakarta, Indonesia
          </div>
          <!-- Default to the left -->
          <strong>Copyright &copy; 2015 - {{ \Carbon\Carbon::now()->format('Y') }}. <a href="{{globalParams('site_name') ? url('/') : 'https://github.com/livecms/liveCMS'}}">{{ globalParams('site_name', 'Live CMS') }}</a>.</strong> {{ globalParams('site_slogan', 'Powered by Laravel Framework.') }}
      </footer>

    </div>
  
  </div>
  <!-- /.content-wrapper -->



  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="/backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="/backend/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/backend/dist/js/app.min.js"></script>
<!-- date js -->
<script src="/backend/plugins/datejs/date.js"></script>
<!-- date-range-picker -->
<script src="/backend/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/backend/plugins/datepicker/locales/bootstrap-datepicker.id.js" charset="UTF-8"></script>
<!-- DataTables -->
<script src="/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/backend/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- Bootstrap Typeahead -->
<script src="/backend/plugins/bootstrap-typeahead/bootstrap3-typeahead.min.js"></script>
<!-- Select2 -->
<script src="/backend/plugins/select2/select2.full.min.js"></script>
<!-- autonumeric -->
<script src="/backend/plugins/autoNumeric/autoNumeric-min.js"></script>
<!-- TinyMCE -->
<script src="/backend/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<!-- Sweet Alert -->
<script src="/backend/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Sweet Alert Form -->
<script src="/backend/plugins/swal-forms/swal-forms.js"></script>
<!-- Sticky Table Header -->
<script src="/backend/plugins/sticky-table-headers/js/jquery.stickyheader.js"></script>
<!-- Responsive Sliders -->
<script src="/backend/plugins/responsive-slides/responsiveslides.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/backend/dist/js/demo.js"></script>
<!-- jscroll js -->
<script src="/frontend/plugins/jscroll-2.3.4/jquery.jscroll.min.js"></script>
<!-- BluImp Image Gallery -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="/backend/plugins/bootstrap-image-gallery/js/bootstrap-image-gallery.min.js"></script>


<script>
  String.prototype.toRp = function(a,b,c,d,e) {
    e=function(f){return f.split('').reverse().join('')};b=e(parseInt(this,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return(a?a:'Rp.\t')+e(d);
  }
  $(function() {
    $.fn.datepicker.defaults.format = "{{ config('liveapp.dateformat', 'dd-MM-yyyy') }}";
    $.fn.datepicker.defaults.language = "en";
    $.fn.datepicker.defaults.todayHighlight = true;
    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.forceParse = false;

    $('.datepicker').datepicker();          

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.fn.modal.Constructor.DEFAULTS.backdrop = 'static';

    $.fn.liveposCurrency = {aSep: '.', aDec: ',', aSign: 'Rp. ', lZero: 'deny'};
    $.fn.liveposNumeric = {aSep: '.', aDec: ',', aSign: '', lZero: 'deny'};

    $('select').select2({width: '100%', tokenSeparators: [',']});    
    
    $('.input-mask.input-mask-currency').autoNumeric('init', $.fn.liveposCurrency);
    $('.input-mask.input-mask-numeric').autoNumeric('init', $.fn.liveposNumeric);

    $('form').submit(function(e) {

      var form = $(this);
      console.log(form);
      form.find('.btn-primary').prop('disabled', true); 
      form.find('.input-mask').each(function(i, e) {
        var v = $(this).autoNumeric('get');
        console.log(v)
        $(this).val(v);
      })
      form.find('.datepicker').each(function(i, e) {
        var v = $(this).val();
        console.log(v)
        console.log($.fn.datepicker.defaults.format)
        d = Date.parseExact(v, [$.fn.datepicker.defaults.format, 'dd-MMM-yyyy']);
        newDate = d.toString('yyyy-M-dd');
        $(this).val(newDate);
      })
      return true;
    })

      var slideToTop = $("<div />");
      slideToTop.html('<i class="fa fa-chevron-up"></i>');
      slideToTop.css({
        position: 'fixed',
        bottom: '40px',
        right: '25px',
        width: '40px',
        height: '40px',
        color: '#eee',
        'font-size': '',
        'line-height': '40px',
        'text-align': 'center',
        'background-color': '#222d32',
        cursor: 'pointer',
        'border-radius': '5px',
        'z-index': '99999',
        opacity: '.7',
        'display': 'none'
      });
      slideToTop.on('mouseenter', function () {
        $(this).css('opacity', '1');
      });
      slideToTop.on('mouseout', function () {
        $(this).css('opacity', '.7');
      });
      $('.wrapper').append(slideToTop);
      $(window).scroll(function () {
        if ($(window).scrollTop() >= 50) {
          if (!$(slideToTop).is(':visible')) {
            $(slideToTop).fadeIn(500);
          }
        } else {
          $(slideToTop).fadeOut(500);
        }
      });
      $(slideToTop).click(function () {
        $("body").animate({
          scrollTop: 0
        }, 500);
      });

  @if(isset($base))
    var table = $('.datatables').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
          url: '{!! action($baseClass.'@data', array_merge(request()->query(), isset($query) ? (array) $query : [])) !!}',
          type: 'POST'
        },
        columns: [
          @foreach(array_keys($fields) as $field) { name: '{{ $field }}', data: '{{ $field }}', sortable: {{ in_array($field, $unsortables) ? 'false' : 'true'}}, searchable: {{ in_array($field, $unsortables) ? 'false' : 'true'}}}, @endforeach
          { name: 'menu', data: 'menu', sortable: false, searchable: false },
        ],
        order: [@foreach($orders as $key => $order) [{{ $key }}, '{{ $order }}']@endforeach],
    }).on( 'draw.dt', function () {
      $(table.table().container())
        .find('div.dataTables_paginate')
        .css( 'display', table.page.info() && table.page.info().pages <= 1 ?
             'none' :
             'block'
      );
    });
  @endif

  @if(isset($useCKEditor) || isset($useTinyMCE))
    tinymce.init({
      selector: 'textarea',
      automatic_uploads: true,
      height: 400,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
      ],
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
      toolbar2: 'print preview media | forecolor backcolor emoticons',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      relative_urls: false,
     });
  @endif

    $('.datatables').on('click', '.btn-need-auth', function () {
      var title   = $(this).data('title') || 'title';
      var method  = $(this).data('method');
      var action  = $(this).data('action');
      var button  = $(this).text() || 'Submit';
      var field   = $(this).data('field') || 'password';
      var obj     = $(this).data('hidden') || {};

      swal.withFormAsync({
        title: title,
        text: '{{trans('backend.needyourpasswordtocontinue')}}',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: button,
        closeOnConfirm: true,
        formFields: [
          { id: field, name: field, type: 'password', placeholder: '{{trans('livecms.password')}}' }
        ]
      }).then(function (context) {
        
        if (context._isConfirm) {
          obj[field] = context.swalForm[field];
          obj['_method'] = method;

          $.post(action, obj, function (data) {
            table.draw(true);
            swal('{{trans('livecms.success')}}', '', 'success');
          }, 'json').error(function (data) {
            error = $.parseJSON(data.responseText)[field][0];
            swal('{{trans('livecms.failed')}}', error.charAt(0).toUpperCase() + error.slice(1), 'error');
          });
        }
      })
    });
    
    $('[role=search]').submit(function(e) {
      e.preventDefault();
      var keywords = $(this).find('input[type=text]').first().val();
      $('#gsc-i-id1').val(keywords);
      $('.gsc-search-button-v2').click();
    })

  })


</script>

@if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::get('sweet_alert.alert') !!});
    </script>
@endif

@yield('script.footer')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '{{env('UA_FRONTEND')}}', 'auto');
    ga('send', 'pageview');

  </script>

</body>
@stop
@yield('templateBody')
</html>