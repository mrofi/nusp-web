<?php

return [

    'home'              => 'Home',
    'dashboard'         => 'Dashboard',
    'user'              => 'Pengguna',
    'setting'           => 'Setting',
    'article'           => 'Artikel',
    'myarticle'         => 'Artikel Saya',
    'staticpage'        => 'Halaman Statis',
    'category'          => 'Kategori',
    'categories'        => 'Kategori',
    'team'              => 'Team',
    'tag'               => 'Tag',
    'permalink'         => 'Permalink',
    'project'           => 'Project',
    'projectcategory'   => 'Kategori Project',
    'client'            => 'Client',
    'clientandproject'  => 'Client & Project',
    'gallery'           => 'Galeri',
    'contact'           => 'Kontak',
    'site'              => 'Website',
    'profile'           => 'Profile',
    'myprofile'         => 'Profile Saya',
    'ban'               => 'Ban User',
    'unban'             => 'Batal Ban',
    'unbanuser'         => 'Batal Ban User',

    'yes'               => 'Ya',
    'no'                => 'Tidak',
    'failed'            => 'Gagal',
    'success'           => 'Sukses',
    'about'             => 'Tentang',
    'aboutme'           => 'Tentang Saya',
    'makeadmin'         => 'Jadikan Admin',
    'name'              => 'Nama',
    'role'              => 'Jabatan',
    'mediasocial'       => 'Media Sosial',
    'username'          => 'Username',
    'password'          => 'Password',
    'password_confirmation' => 'Ulangi Password',
    'yourpassword'      => 'Password Anda',
    'newpassword'       => 'Password Baru',
    'newpassword_confirmation' => 'Ulangi Password',
    'passwordprivilege' => 'Password Akun',
    'email'             => 'Email',
    'key'               => 'Key',
    'value'             => 'Value',
    'type'              => 'Tipe',
    'title'             => 'Judul',
    'jobtitle'          => 'Pekerjaan / Jabatan',
    'content'           => 'Isi',
    'description'       => 'Deskripsi',
    'post'              => 'Tulisan',
    'picture'           => 'Foto Utama',
    'avatar'            => 'Avatar',
    'parent'            => 'Induk',
    'id'                => 'Id',
    'url'               => 'URL',
    'slug'              => 'Slug',
    'background'        => 'Gambar Belakang',
    'author'            => 'Penulis',
    'published at'      => 'Diterbitkan Pada',
    'address'           => 'Alamat',
    'address2'          => 'Alamat 2',
    'city'              => 'Kota / Kabupaten',
    'province'          => 'Propinsi',
    'state'             => 'Negara Bagian',
    'country'           => 'Negara',
    'postcode'          => 'Kode Pos',
    'faximile'          => 'Faximile',
    'telephone'         => 'Telepon',
    'sitename'          => 'Nama Situs',
    'subdomain'         => 'Sub Domain',
    'subfolder'         => 'Sub Folder',
    'viewed'            => 'Dilihat',
    'socials'           => [
                            'github' => 'Github',
                            'linkedin' => 'Linkedin',
                            'facebook' => 'Facebook',
                            'twitter' => 'Twitter',
                            'instagram' => 'Instagram',
                            'google-plus' => 'Google Plus',
                        ],
];
