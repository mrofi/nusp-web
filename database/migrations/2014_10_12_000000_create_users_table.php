<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('username');
            $table->string('email');
            $table->string('password', 60);
            $table->string('avatar')->nullable();
            $table->string('background')->nullable();
            $table->string('jobtitle')->nullable();
            $table->text('socials')->nullable();
            $table->text('about')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('site_id')
                  ->references('id')->on('sites')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
