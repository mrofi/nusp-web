<?php

return [

    'canonical' =>  env('IMAGEMAX_CANONICAL', 'nusp-2'), // YOUR CANONICAL NAME
    'baseurl' => env('IMAGEMAX_BASEURL', 'http://nusp-2.org'), // YOUR BASE URL
    'profiles' => [
        
        'thumb' => '128x128.jpg',
        
        'small' => '230x150.jpg',

        'medium' => '460x300.jpg', // w = 500; h = 300; q = 8; bri = 10
        
        'large' => '920x600.jpg', // w = 800; h = 600
        
        'x-large' => '1380x900.jpg', // w = 1200; h = 900

        'slider' => '630x390.jpg',

        'slider-mini' => '210x130.jpg',
    ],
];
