<?php
/*
 * This file has been generated automatically.
 * Please change the configuration for correct use deploy.
 */
require 'recipe/laravel.php';
define('REPO', 'git@bitbucket.org:mrofi/nusp-web.git');
$servers = [
    'staging' => [
        'server' => '31.220.111.141',
        'user' => 'nusp',
        'path' => '/home/nusp/html',
    ],
    'production' => [
        'server' => 'ipproduction',
        'user' => 'username_production',
        'path' => '/path/to/deploy',
    ],
];
// Set configurations
env('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');
// If we don't need sudo, keep it false
set('writable_use_sudo', false);
set('repository', REPO);
set('shared_files', ['.env']);
set('shared_dirs', [
    'storage/app',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
    
    // app shared
    'public/uploads',
    'public/background',
    'public/users',

]);
set('writable_dirs', ['bootstrap/cache', 'storage']);
// Configure servers
foreach ($servers as $name => $server) {
    
    server($name, $server['server'])
        ->user($server['user'])
        ->password()
        ->env('deploy_path', $server['path']);
}
/**
 * Restart php-fpm on success deploy.
 */
task('artisan:clear-compiled', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan clear-compiled');
    writeln('<info>'.$output.'</info>');
})->desc('Clear compiled done');
task('artisan:optimize', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan optimize');
    writeln('<info>'.$output.'</info>');
})->desc('Optimize done');
task('artisan:elfinder:publish', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan elfinder:publish');
    writeln('<info>'.$output.'</info>');
})->desc('Publishing Elfinder');
task('artisan:cache:clear', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/artisan cache:clear');
    writeln('<info>'.$output.'</info>');
})->desc('Clear cache');
task('after-deploy', [
    'artisan:clear-compiled',
    'artisan:optimize',
    // 'artisan:elfinder:publish',
    'artisan:cache:clear',
])->desc('Deploying done.');
after('success', 'after-deploy');